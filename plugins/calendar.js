;(function(window, $){
	'use strict';

$.fn.extend({

		calendar : function(options){

		     var $this = $(this),

		      calendar = {

		       init: function(container){

		        var self = this;

		        self.container = container;
		        self.currentDate = new Date();
		        self.CDYear = self.currentDate.getFullYear();
		        self.CDMonth = self.currentDate.getMonth();
		        self.dCurrentDate = new Date(self.CDYear, self.CDMonth);
		        self.counter = self.CDMonth;
		        self.generateContainers(self.dCurrentDate);
		        self.bindEvents();

		        console.log(options);

		       },

		       generateContainers: function(date){

		        var self = this,
			         wrap = $('<table class="t_calendar"></table>'),
			         header = $('<thead><tr><th colspan="7" class="tc_header">'+options.months[date.getMonth()]+ ' ' + date.getFullYear() +'</th></tr></thead>'),
			         tbody = '<tbody>',
			         dayLabels = $('<tr></tr>'),
			         prevMonthLabel = self.currentDate.getMonth() == 0 ? options.prevNextLabels[11] : options.prevNextLabels[self.currentDate.getMonth() - 1],
			         nextMonthLabel = self.currentDate.getMonth() == 11 ? options.prevNextLabels[0] : options.prevNextLabels[self.currentDate.getMonth() + 1],
			         footer = $('<footer class="tc_actions"><a href="#" class="tc_prev" data-dir="prev"><span class="tc_calendar_text_link_prev">'+prevMonthLabel+'</span><i class="tc_calendar_arr_prev"></i></a><a href="#" class="tc_next t_hide" data-dir="next"><span class="tc_calendar_text_link_next">'+nextMonthLabel+'</span><i class="tc_calendar_arr_next"></i></a></footer>');

		        for(var i = 0; i < 7; i++)
		         dayLabels.append('<th>'+options.dayLabels[i]+'</th>');
		        
		        header.append(dayLabels);

		        tbody += self.generateMonthMarkup(self.CDYear, self.CDMonth) + '</tbody>';
		        
		        wrap.append(header);
		        wrap.append(tbody);
		        self.container.html(wrap);

		        self.container.after(footer);

		        self.body = self.container.find('tbody');
		        self.prev = footer.children('.tc_prev');
		        self.next = footer.children('.tc_next');
		        self.header = self.container.find('.tc_header');

		       },

		       generateMonthMarkup: function(year, month){

		        var date = new Date(year, month),
		         self = this,
		         template = "<tr>";

		        // fill empty cells
		        var prevMonth = new Date(year, month, 0),
		        	DayOfPrevMonth = prevMonth.getDate(),
		        	prevMonthDates = [];
		        for(var i = 0; i < self.getTableDay(date); i++){
		        	prevMonthDates.push("<td><span class='prev_t_date t_date'>" +DayOfPrevMonth+ "</span></td>");
		        	DayOfPrevMonth--;
		        }

		        template += prevMonthDates.reverse().join();

		        // generate full month
		        while(date.getMonth() == month){
		         if(year == self.CDYear && month == self.CDMonth && date.getDate() == self.currentDate.getDate())
		          template += '<td class="active"><span class="t_date">'+date.getDate()+'</span></td>';
		         else{
		          var isEvent = false;
		          $.each(options.events, function(i, obj){
		           if(obj.date.valueOf() == date.valueOf()){
		           	template += self.getEventTemplateType(obj, date);
		            isEvent = true;
		            return false;
		           }
		          });
		          if(!isEvent){
		              template += '<td><span class="t_date">'+date.getDate()+'</span></td>';
		              isEvent = false;
		          }
		            }
		            if (self.getTableDay(date) % 7 == 6) template += '</tr><tr>';
		            date.setDate(date.getDate()+1);
		        }
		        if(self.getTableDay(date) != 0){
		        	var nextMonthDays = [];
			        for(var i=self.getTableDay(date), j = 1; i<7; i++, j++){
			        	nextMonthDays.push("<td><span class='next_        t_date t_date'>" +j+ "</span></td>");
			        }

		        template += nextMonthDays.join();

		         // for (){
		         // 	template += '<td></td>'; // next month
		         // }
		        }

		        return template;

		       },

		       getEventTemplateType: function(obj, date){

		       	var templates = [

					// templates 0 start

		       		'<td><a href="'+obj.link+'" class="t_calendar_link"><span class="'+obj.acceptedClass+'"><span class="alert_ico_calendar"></span><span class="t_date">'+date.getDate()+'</span><span class="t_status">'+obj.acceptedText+'</span><span class="t_description">'+obj.description+'</span><span class="t_time">'+obj.time+'</span></span></a></td>',

					// templates 1 start

		       		'<td><a href="'+obj.link+'" class="'+obj.mainLink+'"><span class="'+obj.numberIcon+'" data-number-near-icon="'+obj.dataNumberimg+'"></span><span class="t_date">'+date.getDate()+'</span><span class="setting_t_icon"></span><span class="'+obj.iconClass+'" data-t-icon="'+obj.dataicon+'"></span><span class="t_description">'+obj.description+'</span></a></td>',

					// templates 2 start

		       		'<td><a href="'+obj.link+'" class="'+obj.mainLink+'"><span class="'+obj.numberIcon+'" data-number-near-icon="'+obj.dataNumberimg+'"></span><span class="t_date">'+date.getDate()+'</span><span class="'+obj.iconClass+'" data-t-icon="'+obj.dataicon+'"></span><span class="t_description">'+obj.description+'</span></a></td>',

					// templates 3 start

		       		'<td><a href="'+obj.link+'" class="'+obj.mainLink+'"><span class="'+obj.numberIcon+'" data-number-near-icon="'+obj.dataNumberimg+'"></span><span class="t_date">'+date.getDate()+'</span><span class="important_ico"></span><span class="'+obj.iconClass+'" data-t-icon="'+obj.dataicon+'"></span><span class="t_description">'+obj.description+'</span></a></td>',

					// templates 4 start

		       		'<td><a href="'+obj.link+'" class="'+obj.mainLink+'"><span class="'+obj.numberIcon+'" data-number-near-icon="'+obj.dataNumberimg+'"></span><span class="t_date">'+date.getDate()+'</span><span class="'+obj.iconClass+'" data-t-icon="'+obj.dataicon+'"></span><span class="t_time">'+obj.time+'</span><span class="t_description">'+obj.description+'</span></a></td>',

					// templates 5 start

		       		'<td><a href="'+obj.link+'" class="'+obj.mainLink+'"><button class="'+obj.buttonClass+'">'+obj.buttonText+'</button></a></td>',

		       		// templates 6 start

		       		'<td><span class="t_date">'+date.getDate()+'</span><a href="'+obj.link1+'" class="'+obj.mainLink1+'"><span class="'+obj.t_calendar_link1+'">'+obj.description1+'</span></a><a href="'+obj.link2+'" class="'+obj.mainLink2+'"><span class="'+obj.t_calendar_link2+'">'+obj.description2+'</span></a><a href="'+obj.link3+'" class="'+obj.mainLink3+'"><span class="'+obj.t_calendar_link3+'">'+obj.description3+'</span></a></td>',

		       		// templates 7 start

		       		'<td><a href="'+obj.link+'" class="'+obj.mainLink+'"><span class="t_date">'+date.getDate()+'</span><span class="'+obj.iconClass+'" data-t-icon="'+obj.dataicon+'"></span><span class="t_description">'+obj.description+'</span><span class="t_time">'+obj.time+'</span></a></td>',

		       		// templates 8 start

		       		'<td><a href="'+obj.link+'" class="'+obj.mainLink+'"><span class="t_date">'+date.getDate()+'</span><span class="'+obj.iconClass+'" data-t-icon="'+obj.dataicon+'"></span><span class="t_description">'+obj.description+'</span><span class="t_time">'+obj.time+'</span></a><a href="'+obj.link2+'" class="'+obj.mainLink2+'"><span class="'+obj.iconClass2+'" data-t-icon="'+obj.dataicon2+'"></span><span class="t_description">'+obj.description2+'</span><span class="'+obj.shadowClass+'"></span></a></td>',

		       		// templates 9 start

		       		'<td><a href="'+obj.link+'" class="'+obj.mainLink+'"><span class="t_date">'+date.getDate()+'</span><span class="'+obj.iconClass+'" data-t-icon="'+obj.dataicon+'"></span><span class="t_description">'+obj.description+'</span></a><a href="'+obj.link2+'" class="'+obj.mainLink2+'"><span class="'+obj.iconClass2+'" data-t-icon="'+obj.dataicon2+'"></span><span class="t_description">'+obj.description2+'</span><span class="'+obj.shadowClass+'"></span></a><a href="'+obj.link3+'" class="'+obj.mainLink3+'"><span class="'+obj.iconClass3+'" data-t-icon="'+obj.dataicon3+'"></span><span class="t_description">'+obj.description3+'</span><span class="'+obj.shadowClass+'"></span></a></td>',

		       	];


		       	return templates[obj.templateType];

		       },

		       getTableDay: function(date){
		        var day = date.getDay();
		          if (day == 0) day = 7;
		          return day - 1;
		       },

		       bindEvents: function(){

		        var self = this;
		        self.prev.add(self.next).on('click', {self: self}, self.changeMonthHandler);

		       },

		       changeMonthHandler: function(event){
		        event.preventDefault();

		        var self = event.data.self,
		         direction = $(this).data('dir'),
		         ddMonth = self.dCurrentDate.getMonth();

		        if(direction == "prev"){
		         self.dCurrentDate.setMonth(--ddMonth);
		         --self.counter;
		         if(self.counter < 0) self.counter = 11;
		        }
		        else{
		         self.dCurrentDate.setMonth(++ddMonth);
		         ++self.counter;
		         if(self.counter > 11) self.counter = 0;
		        }
		        
		        var prevMonthLabel = self.counter == 0 ? 11 : (self.counter-1) % 12;
		        var nextMonthLabel = self.counter == 11 ? 0 : (self.counter+1) % 12;

		        self.prev.html('<span class="tc_calendar_text_link_prev">'+options.prevNextLabels[prevMonthLabel]+'</span><i class="tc_calendar_arr_prev"></i>');
		        self.next.html('<span class="tc_calendar_text_link_next">'+options.prevNextLabels[nextMonthLabel]+'</span><i class="tc_calendar_arr_next"></i>');
		        // self.prev.html('<span>&#171;</span>' + options.prevNextLabels[prevMonthLabel]);
		        // self.next.html(options.prevNextLabels[nextMonthLabel] + '<span>&#187;</span>');

		        if(self.dCurrentDate.getMonth() == self.CDMonth && self.dCurrentDate.getFullYear() == self.CDYear)
		         self.next.addClass('t_hide');
		        else
		         self.next.removeClass('t_hide');


		        self.header.html(options.months[self.dCurrentDate.getMonth()] + " " + self.dCurrentDate.getFullYear());
		        self.body.html(self.generateMonthMarkup(self.dCurrentDate.getFullYear(), self.dCurrentDate.getMonth()));
		       }

		      }

		     return $this.each(function(){

		      calendar.init($(this));

		     });

		    }


		});

})(window, jQuery);

	


// footer = $('<footer class="tc_actions"><a href="#" class="tc_prev" data-dir="prev"><span>&#171;</span>'+prevMonthLabel+'</a><a href="#" class="tc_next t_hide" data-dir="next">'+nextMonthLabel+'<span>&#187;</span></a><footer>');
