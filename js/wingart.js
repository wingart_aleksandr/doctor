// $(function(){
// 	var viewportmeta = document.querySelector && document.querySelector('meta[name="viewport"]'),
// 	ua = navigator.userAgent,

// 	gestureStart = function () {viewportmeta.content = "width=device-width, minimum-scale=0.25, maximum-scale=1.6";},

// 	scaleFix = function () {
// 		if (viewportmeta && /iPhone|iPad/.test(ua) && !/Opera Mini/.test(ua)) {
// 			viewportmeta.content = "width=device-width, minimum-scale=1.0, maximum-scale=1.0";
// 			document.addEventListener("gesturestart", gestureStart, false);
// 		}
// 	};
	
// 	scaleFix();
// });
// var ua=navigator.userAgent.toLocaleLowerCase(),
//  regV = /ipod|ipad|iphone/gi,
//  result = ua.match(regV),
//  userScale="";
// if(!result){
//  userScale=",user-scalable=0"
// }
// document.write('<meta name="viewport" content="width=device-width,initial-scale=1.0'+userScale+'">')

// ЕСЛИ ПРОЕКТ РЕСПОНСИВ ТО ВСЕ ЧТО ВЫШЕ НУЖНО РАССКОМЕНТИРОВАТЬ. СКРИПТ ВЫШЕ ПРЕДНАЗНАЧЕН ДЛЯ КОРРЕКТНОГО ОТОБРАЖЕНИЯ ВЕРСТКИ ПРИ СМЕНЕ ОРИЕНТАЦИИ НА ДЕВАЙСАХ




$(document).ready(function(){




		// =======================================show_block start=======================================
				 
			    if($(".click_for_edit").length){
			    	$(".click_for_edit").on("click", function(event){
			    		event.preventDefault();
			    		var $this = $(this);
			    		$this
			    		.parents(".editing_unit_box_item")
			    		.addClass("current_edit")
			    		.find(".redaction_container_write")
			    		.slideDown('fast')
			    		.parents(".editing_unit_box_item")
			    		.find(".redaction_box")
			    		.slideUp('fast');
			    	});
			    	$(".cancel_editing").on("click", function(event){
			    		event.preventDefault();
			    		var $this = $(this);
			    		$this
			    		.parents(".editing_unit_box_item")
			    		.removeClass("current_edit")
			    		.find(".redaction_container_write")
			    		.slideUp('fast')
			    		.parents(".editing_unit_box_item")
			    		.find(".redaction_box")
			    		.slideDown('fast');
			    	});
			    }

		// =======================================show_block end=======================================




		// =======================================show_block2 start=======================================
				 
			    if($(".link_developments").length){
			    	$(".link_developments").on("click", function(event){
			    		event.preventDefault();
			    		var $this = $(this),
			    			parenTs = $this.parents(".container_filter_box"),
			    			linkOnClick = parenTs.find(".link_developments"),
			    			boxWhichShow = parenTs.find(".container_filter_hidden");
			    		
			    		parenTs.toggleClass("active")
			    		linkOnClick.toggleClass("active")
			    		boxWhichShow.slideToggle('fast');
			    	});
			    }

		// =======================================show_block2 end=======================================




		// =======================================activity_link start=======================================
				
				if($(".eyes_link").length){					
					$(".eyes_link").on("click", function(event){
						$(this).toggleClass('active');
						event.preventDefault();
					});
				}

		// =======================================activity_link end=======================================



		



		/* ------------------------------------------------
		FOCUS INPUT START
		------------------------------------------------ */

			   $(".alphabet_input").live("focus blur", function(e){
			      var $this = $(this),
			          parentInput = $(".search_box");
			      setTimeout(function(){
			        $this.toggleClass("focused_child", $this.is(":focus"));
			        $this.parent(parentInput).toggleClass("focused_parent", $this.is(":focus"));
			      }, 0);
			    });

		/* ------------------------------------------------
		FOCUS INPUT END
		------------------------------------------------ */




});