//  /*================================================>  
//                                 >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>  INCLUDE AND INITIALIZE Plugins START  <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
//  <================================================*/




		var tabs = $(".tabs"),
			matchheight = $("[data-mh]"),
		    styler = $(".styler"),
			datepicker = $(".datepicker"),
			calendar = $(".calendar"),
			tooltip = $(".tooltip"),
			tooltipTwo = $(".tooltip2"),
			tooltipThree = $(".tooltip3"),
			tooltipFour = $(".tooltip4"),
			tooltipFive = $(".tooltip5"),
			popup = $("[data-popup]"),
			windowW = $(window).width(),
			windowH = $(window).height();


			if(matchheight.length){
					include("plugins/jquery.matchHeight-min.js");
			}
			if(styler.length){
					include("plugins/formstyler/formstyler.js");
			}
			if(calendar.length){
					include("plugins/calendar.js");
			}
			if(tabs.length){
					include("plugins/easy-responsive-tabs/jquery-ui.js");
			}
			if( $(".date").length || $(".phone").length){
					include("plugins/maskedInput.js");
			}
			if(popup.length){
					include('plugins/arcticmodal/jquery.arcticmodal.js');
			}
			if(datepicker.length){
					include('plugins/datepicker/jquery-ui.js');
					include('plugins/datepicker/datepicker-ru.js');
			}
			if(tooltip.length || tooltipTwo.length || tooltipThree.length || tooltipFour.length || tooltipFive.length){
					include("plugins/tooltip/jquery.tooltipster.js");
			}




			function include(url){ 

					document.write('<script src="'+ url + '"></script>'); 

			}

		


		$(document).ready(function(){




			/* ------------------------------------------------
			FORMSTYLER START
			------------------------------------------------ */

					if (styler.length){
						styler.styler();
					}

			/* ------------------------------------------------
			FORMSTYLER END
			------------------------------------------------ */



			
			/* ------------------------------------------------
			TABS START
			------------------------------------------------ */

					if(tabs.length){
						tabs.tabs();
					}

			/* ------------------------------------------------
			TABS END
			------------------------------------------------ */




			/* ------------------------------------------------
			POPUP START
			------------------------------------------------ */

					if(popup.length){
						popup.on('click',function(){
						    var modal = $(this).data("popup");
						    $(modal).arcticmodal({
								afterOpen: function(){
									if($(modal).find('#datepicker').length){
										$(modal).find( "#datepicker" ).datepicker({
											dateFormat: "d M, yy"
										});
									}
								}
							});
						});
					};

			/* ------------------------------------------------
			POPUP END
			------------------------------------------------ */




			/* ------------------------------------------------
			DATAPICKER START
			------------------------------------------------ */

					if(datepicker.length){
						$( "#datepicker" ).datepicker({
							dateFormat: "d M, yy"
						});
						$( "#datepicker2" ).datepicker();
						$( "#datepicker3" ).datepicker({
					      defaultDate: +7,
					      numberOfMonths: 1,
					      dateFormat: "M d, yy",
					      onClose: function( selectedDate ) {
					        $( "#datepicker4" ).datepicker( "option", "minDate", selectedDate );
					      }
					    });
					    $( "#datepicker4" ).datepicker({
					      defaultDate: +7,
					      numberOfMonths: 1,
					      dateFormat: "M d, yy",
					      onClose: function( selectedDate ) {
					        $( "#datepicker3" ).datepicker( "option", "maxDate", selectedDate );
					      }
					    });
					}

			/* ------------------------------------------------
			DATAPICKER END
			------------------------------------------------ */




			/* ------------------------------------------------
			MASKED START
			------------------------------------------------ */
				    
				    if( $(".date").length || $(".phone").length){
				        $(".date").mask("99/99/9999",{placeholder:"mm/dd/yyyy"});
				        $(".phone").mask("+7(999) 999-99-99");
				    }

			/* ------------------------------------------------
			MASKED END
			------------------------------------------------ */




			/* ------------------------------------------------
			CALENDAR START
			------------------------------------------------ */

				   var 	$calendar = $('#calendar'),
				   		$calendarTwo = $('#calendar2');

				    if($calendar.length){

						$calendar.calendar({
					    	months : ['Январь','Февраль','Март','Апрель','Май','Июнь','Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь'],
					    	prevNextLabels : ['Янв', 'Фев', 'Мар', 'Апр', 'Май', 'Июн', 'Июл', 'Авг', 'Сен', 'Окт', 'Ноя', 'Дек'],
					    	dayLabels : ['Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб', 'Вс'],
							events: [

										{
											'date': new Date(2016, 1, 10),
											'link': 'http://www.example.ru',
											'acceptedClass': 'accepted',
											'acceptedText': 'Согласован',
											'description': 'Вопросы профпатологии...',
											'time': '12:30',
											'templateType' : 0
										},

										{
											'date': new Date(2016, 1, 11),
											'link': 'http://www.example.ru',
											'acceptedClass': 'to_accepted',
											'acceptedText': 'Согласование',
											'description': 'Лучевая диагностика диаг...',
											'time': '14:30',
											'templateType' : 0
										},

										{
											'date': new Date(2016, 1, 12),
											'link': 'http://www.example.ru',
											'acceptedClass': 'not_accepted',
											'acceptedText': 'Не Согласован',
											'description': 'По семейным обстоятельствам...',
											'time': '12:30',
											'templateType' : 0
										},

										{
											'date': new Date(2016, 1, 13),
											'link': 'http://www.example.ru',
											'mainLink' : 't_calendar_link item2',
											'iconClass' : 'ok_icon',
											'dataicon' : 'ok_icon',
											'description': 'Лучевая диагностика',
											'time': '15:00',
											'templateType' : 7
										},

										{
											'date': new Date(2016, 1, 14),
											'link': 'http://www.example.ru',
											'mainLink' : 't_calendar_link item2',
											'iconClass' : 'hourglass_icon',
											'dataicon' : 'hourglass_icon',
											'description': 'Лучевая диагностика диаг...',
											'time': '14:30',
											'templateType' : 7
										},

										{
											'date': new Date(2016, 1, 15),
											'link': 'http://www.example.ru',
											'mainLink' : 't_calendar_link item3',
											'iconClass' : 'del_icon',
											'dataicon' : 'del_icon',
											'description': 'Лучевая диагностика диаг...',
											'time': '14:30',
											'templateType' : 7
										},

										{
											'date': new Date(2016, 1, 16),
											'link': 'http://www.example.ru',
											'link2': 'http://www.example.ru',
											'mainLink' : 't_calendar_link item2 item4',
											'mainLink2' : 't_calendar_link_it2',
											'iconClass' : 'ok_icon',
											'dataicon' : 'ok_icon',
											'iconClass2' : 'hourglass_icon',
											'dataicon2' : 'hourglass_icon',
											'description': 'Вопросы профпатологии...',
											'description2': 'Вибрационная',
											'shadowClass' : 't_shadow',
											'time': '12:30',
											'templateType' : 8
										},

										{
											'date': new Date(2016, 1, 17),
											'link': 'http://www.example.ru',
											'link2': 'http://www.example.ru',
											'link3': 'http://www.example.ru',
											'mainLink' : 't_calendar_link item2 item4 item5',
											'mainLink2' : 't_calendar_link_it2 it3',
											'mainLink3' : 't_calendar_link_it2',
											'iconClass' : 'hourglass_icon',
											'dataicon' : 'hourglass_icon',
											'iconClass2' : 'ok_icon',
											'dataicon2' : 'ok_icon',
											'iconClass3' : 'hourglass_icon',
											'dataicon3' : 'hourglass_icon',
											'description': 'Лучевая диагностика диаг...',
											'description2': 'По семейным',
											'description3': 'Вибрационная',
											'shadowClass' : 't_shadow',
											'templateType' : 9
										}

							]
					    });

					}

					if($calendarTwo.length){

						$calendarTwo.calendar({
					    	months : ['Январь','Февраль','Март','Апрель','Май','Июнь','Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь'],
					    	prevNextLabels : ['Янв', 'Фев', 'Мар', 'Апр', 'Май', 'Июн', 'Июл', 'Авг', 'Сен', 'Окт', 'Ноя', 'Дек'],
					    	dayLabels : ['Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб', 'Вс'],
							events: [

										{
											'date': new Date(2016, 1, 1),
											'link': 'http://www.example.ru',
											'mainLink' : 't_calendar_link2',
											'numberIcon' : 't_calendar_number2',
											'dataNumberimg' : '№2',
											'iconClass' : 't_icon',
											'dataicon' : 't-icon1',
											'description': 'Основы законодательства РФ в сфере охра...',
											// 'addTEvent': 'Добавить событие',
											'templateType': 1
										},

										{
											'date': new Date(2016, 1, 2),
											'link': 'http://www.example.ru',
											'mainLink' : 't_calendar_link3',
											'numberIcon' : 't_calendar_number3',
											'dataNumberimg' : '№19',
											'iconClass' : 't_icon',
											'dataicon' : 't-icon2',
											'description': 'Практическое<br> задание',
											'templateType': 2
										},

										{
											'date': new Date(2016, 1, 3),
											'link': 'http://www.example.ru',
											'mainLink' : 't_calendar_link4',
											'numberIcon' : 't_calendar_number4',
											'dataNumberimg' : '№20',
											'iconClass' : 't_icon',
											'dataicon' : 't-icon3',
											'description': 'Лучевая диагностика доброкачественн...',
											'templateType': 3
										},

										{
											'date': new Date(2016, 1, 4),
											'link': 'http://www.example.ru',
											'mainLink' : 't_calendar_link5',
											'numberIcon' : 't_calendar_number5',
											'dataNumberimg' : '№4',
											'iconClass' : 't_icon',
											'dataicon' : 't-icon4',
											'description': 'Организация леч-профилактич...',
											'templateType': 4,
											'time': '12:30'
										},

										{
											'date': new Date(2016, 1, 5),
											'link': 'http://www.example.ru',
											'mainLink' : 't_calendar_link6',
											'buttonClass' : 'add_t_event btn_pink',
											'buttonText' : 'Добавить<br> событие',
											'templateType': 5,
										},
										{
											'date': new Date(2016, 1, 6),
											'link': 'http://www.example.ru',
											'mainLink' : 't_calendar_link7',
											'numberIcon' : 't_calendar_number7',
											'dataNumberimg' : '№3',
											'iconClass' : 't_icon',
											'dataicon' : 't-icon5',
											'description': 'Социальная гигиена и организация медицинской...',
											'templateType': 2
										},
										{
											'date': new Date(2016, 1, 7),
											'link1': 'http://www.example.ru',
											'link2': 'http://www.example.ru',
											'link3': 'http://www.example.ru',
											'mainLink1' : 't_calendar_link8',
											'mainLink2' : 't_calendar_link9',
											'mainLink3' : 't_calendar_link10',
											't_calendar_link1' : 't_calendar_link8_item',
											't_calendar_link2' : 't_calendar_link9_item',
											't_calendar_link3' : 't_calendar_link10_item',
											'description1': 'Основы з...',
											'description2': '12:30',
											'description3': 'Организация...',
											'templateType': 6
										}

							]
					    });

					}

			/* ------------------------------------------------
			CALENDAR END
			------------------------------------------------ */




			/* ------------------------------------------------
			TOOLTIP START
			------------------------------------------------ */

					if(tooltip.length || tooltipTwo.length || tooltipThree.length || tooltipFour.length || tooltipFive.length){

						tooltip.tooltipster({
							maxWidth: 180
						});

						tooltipTwo.tooltipster({
							contentAsHTML: true,
							theme: 'tooltip2_item',
							multiple : true
			            });

			            tooltipThree.tooltipster({
							contentAsHTML: true,
							theme: 'tooltip3_item',
							multiple : true
			            });

			            tooltipFour.tooltipster({
							contentAsHTML: true,
							theme: 'tooltip4_item',
							multiple : true,
							position : 'bottom-right',
							offsetX : 15,
							offsetY : -20,
							hideOnClick : false,
							interactive: true,
							trigger: 'click'
			            });
					            tooltipFour.on("click ontouchstart", function(){
					            	tooltipFour.removeClass('active');
					            	$(this).addClass('active');
					            });


					    tooltipFive.tooltipster({
							contentAsHTML: true,
							theme: 'tooltip5_item',
							multiple : true,
							position : 'bottom-right',
							offsetX : 15,
							offsetY : -10,
							hideOnClick : false,
							interactive: true,
							trigger: 'click'
			            });
					            tooltipFive.on("click ontouchstart", function(event){
					            	$(this).toggleClass('active');
					            	event.preventDefault();
					            });


						$(document).on("click ontouchstart", function(event) {
					      if ($(event.target).closest(".tooltip4 , .tooltipster-base , .tooltipFour_link").length) return;
					      if ($(event.target).closest(".tooltip5 , .tooltipster-base , .tooltipFive_link").length) return;
					      $(".tooltip4").removeClass("active");
					      $(".tooltip5").removeClass("active");
					      event.stopPropagation();
					    });
					}

			/* ------------------------------------------------
			TOOLTIP END
			------------------------------------------------ */



		});




//  /*================================================>  
//                                 >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>  INCLUDE AND INITIALIZE Plugins END    <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
//  <================================================
